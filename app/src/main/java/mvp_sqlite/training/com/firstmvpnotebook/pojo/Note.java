package mvp_sqlite.training.com.firstmvpnotebook.pojo;

public class Note {

    private int id;
    private String noteText;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoteText() {
        return noteText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    @Override
    public String toString() {
        return noteText;
    }
}
