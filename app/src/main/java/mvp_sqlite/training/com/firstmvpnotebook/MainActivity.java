package mvp_sqlite.training.com.firstmvpnotebook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import mvp_sqlite.training.com.firstmvpnotebook.adapters.NoteAdapter;
import mvp_sqlite.training.com.firstmvpnotebook.db.NoteDAO;
import mvp_sqlite.training.com.firstmvpnotebook.mvp.NotesPresenter;
import mvp_sqlite.training.com.firstmvpnotebook.mvp.NotesViewInterface;
import mvp_sqlite.training.com.firstmvpnotebook.pojo.Note;

public class MainActivity extends AppCompatActivity implements NotesViewInterface {

    @BindView(R.id.new_note_text) EditText newNoteText;
    @BindView(R.id.note_list)     ListView noteList;

    @OnClick(R.id.add_new_note_button)
    void setOnButtonClick(){
        presenter.onButtonClicked();
    }
    @OnItemClick(R.id.note_list)
    void setOnItemListClick(int position){
        presenter.onListItemClicked(position);
    }
    @OnItemLongClick(R.id.note_list)
    boolean setOnItemLongClick(int position){
        presenter.onListItemLongClicked(position);
        return true;
    }

    private NotesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new NotesPresenter(new NoteDAO(this), this);
        presenter.refreshDatabaseElements();
    }

    @Override
    public void showAllElementList(List<Note> notes) {
        NoteAdapter noteAdapter = new NoteAdapter(this, notes);
        noteList.setAdapter(noteAdapter);
    }

    @Override
    public String getTextFromEditText() {
        return newNoteText.getText().toString();
    }
}
