package mvp_sqlite.training.com.firstmvpnotebook.mvp;


import java.util.List;

import mvp_sqlite.training.com.firstmvpnotebook.pojo.Note;

public class NotesPresenter implements NotesPresenterInterface, NotesInteractorInterface.OnFinishedListener{

    private NotesInteractorInterface notesInteractorInterface;
    private NotesViewInterface notesViewInterface;

    public NotesPresenter(NotesInteractorInterface notesInteractorInterface, NotesViewInterface notesViewInterface) {
        this.notesInteractorInterface = notesInteractorInterface;
        this.notesViewInterface = notesViewInterface;
    }

    @Override
    public void onButtonClicked() {
       String noteText = notesViewInterface.getTextFromEditText();
       Note note = new Note();
       note.setNoteText(noteText);
       notesInteractorInterface.insertNote(note);
       notesInteractorInterface.getAllNotes(this);
    }

    @Override
    public void onListItemClicked(int position) {
        Note note = notesInteractorInterface.getAllNotes().get(position);
        notesInteractorInterface.removeNoteById(note.getId());
        notesInteractorInterface.getAllNotes(this);
    }

    @Override
    public void onListItemLongClicked(int position) {
        Note note = notesInteractorInterface.getAllNotes().get(position);
        String newText = notesViewInterface.getTextFromEditText();
        note.setNoteText(newText);
        notesInteractorInterface.updateNote(note);
        notesInteractorInterface.getAllNotes(this);
    }

    @Override
    public void refreshDatabaseElements() {
        notesInteractorInterface.getAllNotes(this);
    }

    @Override
    public void sendListOfValues(List<Note> notesList) {
        notesViewInterface.showAllElementList(notesList);
    }
}
